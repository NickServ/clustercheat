--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return function()
	local window = vgui.Create("DFrame")
	window:SetSize(360, 480)
	window:Center()
	window:SetTitle(brand)
	window:SetDeleteOnClose(false)
	window:SetScreenLock(true)
	window:SetMinHeight(360/2)
	window:SetMinWidth(480/2)
	window:SetSizable(true)
	window:SetIcon("icon16/script.png")
	window:MakePopup()
	
	local menu_bar = vgui.Create("DMenuBar", window)
	menu_bar:DockMargin(-3, -5, -3, 0)
	menu_bar:Dock(TOP)
	-- "File"
	local mb_file = menu_bar:AddMenu("File")
	local open_prompt
	mb_file:AddOption("Open...", function()
		local path = "GAME"
		
		open_prompt = vgui.Create("DFrame")
		local prompt = open_prompt
		prompt:SetSize(598, 362)
		prompt:SetMinWidth(598/2)
		prompt:SetMinHeight(362/2)
		prompt:SetSizable(true)
		prompt:Center()
		prompt:DockPadding(5, 5, 5, 5)
		prompt:SetTitle("Open script")
		prompt:SetIcon("icon16/folder_go.png")
		prompt:MakePopup()
		-- I'd put the browser right here but then I get errors because
		-- it references stuff defined after it.
		--[[
		local cluster = vgui.Create("DPanel", prompt)
		cluster_top:DockMargin(0, 5, 0, 0)
		cluster:SetPaintBackground(false)
		cluster:Dock(BOTTOM)
		--]]
		local cluster_bottom = vgui.Create("DPanel", prompt)
		cluster_bottom:DockMargin(0, 5, 0, 0)
		cluster_bottom:SetPaintBackground(false)
		cluster_bottom:Dock(BOTTOM)
		local cluster_top = vgui.Create("DPanel", prompt)
		cluster_top:DockMargin(0, 5, 0, 0)
		cluster_top:SetPaintBackground(false)
		cluster_top:Dock(BOTTOM)
		local filename_label = vgui.Create("DLabel", cluster_top)
		filename_label:SetText("Filename")
		filename_label:SetContentAlignment(6)
		filename_label:Dock(LEFT)
		local filename_entry = vgui.Create("DTextEntry", cluster_top)
		filename_entry:DockMargin(5, 0, 5, 0)
		filename_entry:Dock(FILL)
		local open_button = vgui.Create("DButton", cluster_top)
		open_button:SetText("Open")
		open_button:SetDisabled(true)
		open_button.DoClick = function()
			-- Commented out in case something happens and the file
			-- browser doesn't work or something
			local filename = filename_entry:GetText()
			--if file.Exists(filename, browser_path) then
				open_button:SetDisabled(true)
				prompt:Close()
				mod_load(filename, path)
			--else
				--print('File "'..tostring(filename)..'" does not exist.')
			--end
		end
		open_button:Dock(RIGHT)
		filename_entry.OnChange = function(contents)
			if contents and contents ~= "" then
				open_button:SetDisabled(false)
			end
		end
		local filetype_label = vgui.Create("DLabel", cluster_bottom)
		filetype_label:SetText("Path")
		filetype_label:SetContentAlignment(6)
		filetype_label:Dock(LEFT)
		local path_entry = vgui.Create("DComboBox", cluster_bottom)
		path_entry:AddChoice("GAME")
		path_entry:AddChoice("LUA")
		path_entry:AddChoice("DATA")
		path_entry:AddChoice("MOD")
		path_entry:SetValue(path)
		-- OnSelect would be here but the browser comes after, so I get
		-- errors if I put it here.
		path_entry:DockMargin(5, 0, 5, 0)
		path_entry:Dock(FILL)
		local cancel_button = vgui.Create("DButton", cluster_bottom)
		cancel_button:SetText("Cancel")
		cancel_button.DoClick = function()
			prompt:Close()
		end
		cancel_button:Dock(RIGHT)
		local browser = vgui.Create("DFileBrowser", prompt)
		browser:SetPath(path)
		--browser:SetBaseFolder(path)
		browser:SetBaseFolder("/")
		browser:SetOpen(true)
		browser:SetCurrentFolder("lua/"..mod_path)
		browser.OnSelect = function(panel, file_path)
			-- The order of arguments in this function is BACKWARDS from
			-- what it says on the wiki. What the fuck?
			filename_entry:SetText(file_path)
			if file_path then
				open_button:SetDisabled(false)
			else
				open_button:SetDisabled(true)
			end
		end
		browser:SetPaintBackground(false)
		-- For some reason the browser overlays the title bar if I don't
		-- do this...
		browser:DockMargin(0, 18+5, 0, 0)
		browser:Dock(FILL)
		path_entry.OnSelect = function(self, index, value)
			browser:SetPath(value)
		end
	end):SetIcon("icon16/folder_go.png")
	mb_file:AddOption("Quit...", function()
		local prompt = vgui.Create("DFrame")
		prompt:SetSize(200, 100)
		prompt:Center()
		prompt:SetTitle("Confirm quit")
		prompt:SetIcon("icon16/help.png")
		local text1 = vgui.Create("DLabel", prompt)
		text1:SetText("Are you sure you want to quit?")
		text1:SetContentAlignment(5)
		text1:Dock(TOP)
		local text2 = vgui.Create("DLabel", prompt)
		text2:SetText("All modules will be unloaded.")
		text2:SetContentAlignment(5)
		text2:Dock(TOP)
		
		local container = vgui.Create("Panel", prompt)
		container:Dock(BOTTOM)
		
		local button_yes = vgui.Create("DButton", container)
		button_yes:SetText("Yes")
		button_yes.DoClick = function()
			quit()
			hook_remove("PlayerButtonDown", "MenuOpen")
			if window then
				window:SetDeleteOnClose(true)
				window:Remove()
			end
			if open_prompt then
				open_prompt:Remove()
			end
			prompt:Close()
		end
		button_yes:Dock(LEFT)
		
		local button_no = vgui.Create("DButton", container)
		button_no:SetText("Cancel")
		button_no.DoClick = function()
			prompt:Close()
		end
		button_no:Dock(RIGHT)
		
		prompt:MakePopup()
	end)
	-- "Help"
	local mb_about = menu_bar:AddMenu("Help")
	mb_about:AddOption("About", function()
		local window = vgui.Create("DFrame")
		window:SetSize(400, 300)
		window:Center()
		window:SetTitle("About "..brand)
		window:SetIcon("icon16/information.png")
		for i=1, #credits do
			local label = vgui.Create("DLabel", window)
			label:SetText(credits[i])
			if i==1 then label:SetFont("DermaLarge") end
			label:SetContentAlignment(5)
			label:Dock(TOP)
		end
		window:InvalidateChildren()
		window:SizeToChildren(true, true)
		window:MakePopup()
	end):SetIcon("icon16/star.png")
	
	local scroller = vgui.Create("DScrollPanel", window)
	scroller:Dock(FILL)
	
	mod_list = mod_list or {}
	
	mod_list_add = function(filename, stop_func, data, internal)
		local index = #mod_list+1
		if internal then index = 0 end
		local panel = vgui.Create("DPanel", scroller)
		panel:SetZPos(index)
		panel:DockMargin(0, 3, 0, 1)
		panel:DockPadding(3, 3, 3, 3)
		panel:Dock(TOP)
		
		-- Add widgets
		local label_cluster = vgui.Create("DPanel", panel)
		label_cluster:SetPaintBackground(false)
		label_cluster:Dock(TOP)
		
		local label = vgui.Create("DLabel", label_cluster)
		if internal then
			label:SetText(data)
		elseif data.name then
			label:SetText("("..data.name..") "..filename)
		else
			label:SetText(filename)
		end
		if (not internal) and data.description then
			label:SetTooltip(data.description)
		end
		label:SetDark(1)
		--label:DockMargin(0, 3, 0, 3)
		label:Dock(FILL)
		
		if not internal then
			local stop_button = vgui.Create("DButton", label_cluster)
			if stop_func then
				stop_button:SetText("Stop")
			else
				stop_button:SetText("Close")
			end
			stop_button.DoClick = function()
				mod_list_remove(filename)
			end
			--stop_button:DockMargin(0, 3, 0, 3)
			stop_button:Dock(RIGHT)
		end
		
		panel:InvalidateChildren()
		panel:SizeToChildren(false, true)
		-- Done with the widgets
		
		local entry = {
			stop_func = stop_func,
			name = name,
			panel = panel,
			index = index,
			label = label,
			stop_button = stop_button
		}
		if not internal then mod_list[filename] = entry end
		return entry
	end
	
	mod_list_remove = function(filename)
		local entry = mod_list[filename]
		if entry and stop_funcs[filename] then
			mod_stop(filename)
		end
		entry.panel:Remove()
	end
	
	mod_list_summon = input.GetKeyCode("\\")
	hook_add("PlayerButtonDown", "MenuOpen", function(ply, button)
		if button == mod_list_summon then
			if window:IsValid() then
				window:Show()
			else
				print("PANIC! "..brand.."'s window somehow was lost! Quitting!")
				hook_remove("PlayerButtonDown", "MenuOpen")
				quit()
			end
		end
	end)
	
	local internal_entry = mod_list_add(nil, nil, brand, true)
	
	local binder_cluster = vgui.Create("DPanel", internal_entry.panel)
	binder_cluster:SetPaintBackground(false)
	binder_cluster:Dock(TOP)
	local binder_label = vgui.Create("DLabel", binder_cluster)
	binder_label:SetText("Open menu")
	binder_label:SetDark(1)
	binder_label:Dock(FILL)
	local binder = vgui.Create("DBinder", binder_cluster)
	binder.OnChange = function(key_num)
		mod_list_summon = binder:GetValue()
	end
	binder:SetSelectedNumber(mod_list_summon)
	binder:Dock(RIGHT)
	
	internal_entry.panel:InvalidateChildren()
	internal_entry.panel:SizeToChildren(false, true)
end
