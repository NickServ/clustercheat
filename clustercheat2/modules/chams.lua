--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Chams",
	description = "Highlights players and entities through walls.",
	license = "CC0 1.0 Universal",
	icon16 = "icon16/status_online.png",
	start = function(filename, panel)
		local show_players = true
		local show_entities = true
		local suppress_lighting = false
		local always_show = true
		local range_enabled = false
		local range = 1000
		local classes = {
			--printer = Color(unpack(colors.info)),
			--clicker = Color(unpack(colors.info)),
			--miner = Color(unpack(colors.info)),
			--spawned_weapon = Color(unpack(colors.warning)),
			--shipment = Color(unpack(colors.warning)),
			--spawned_money = Color(unpack(colors.success)),
			--durgz = Color(unpack(colors.success))
		}
		local material = CreateMaterial(
			"Chameleon",
			"VertexLitGeneric",
			{
				["$basetexture"] = "models/debug/debugwhite",
				["$model"] = 1,
				["$ignorez"] = 1
			}
		)
		local draw = function(ent, color)
			if not color then color = Color(255, 255, 255, 255) end
			
			render.SetColorModulation(color.r/255, color.g/255, color.b/255)
			render.SuppressEngineLighting(suppress_lighting)
			if always_show then
				render.MaterialOverride(Material("models/debug/debugwhite"))
				cam.IgnoreZ(true)
			else
				render.MaterialOverride(Material("!Chameleon"))
			end
			ent:DrawModel()
			
			render.SetColorModulation(1, 1, 1)
			render.SuppressEngineLighting(false)
			cam.IgnoreZ(false)
			render.MaterialOverride()
			if not always_show then
				ent:DrawModel()
			end
		end
		hook_add("RenderScreenspaceEffects", "Chams", function()
			if show_players then
				for _, ply in pairs(player.GetAll()) do
					if ply:IsValid() and ply ~= LocalPlayer() and ply:Alive() then
						if range_enabled then
							if EyePos():DistToSqr(ply:GetPos()) > range*range then continue end
						end
						
						cam.Start3D(EyePos(), EyeAngles())
							draw(ply, team.GetColor(ply:Team()) or Color(unpack(colors.primary)))
							
							local weapon = ply:GetActiveWeapon()
							if weapon and weapon:IsValid() then
								draw(weapon, team.GetColor(ply:Team()) or Color(unpack(colors.primary)))
							end
						cam.End3D()
					end
				end
			end
			if show_entities and #classes then
				for class, color in pairs(classes) do
					-- I'd use FindByClass but its wildcard support is poor.
					for _, ent in pairs(ents.GetAll()) do
						if color and string.match(tostring(ent:GetClass()), class) then
							if range_enabled then
								if EyePos():DistToSqr(ent:GetPos()) > range*range then break end
							end
							cam.Start3D(EyePos(), EyeAngles())
								draw(ent, color)
							cam.End3D()
						end
					end
				end
			end
		end)
		if panel and panel.IsValid and panel:IsValid() then
			local mixer = vgui.Create("DColorMixer", panel)
			mixer:SetPalette(false)
			mixer:SetAlphaBar(true)
			mixer:SetColor(Color(unpack(colors.primary)))
			mixer:DockMargin(0, 3, 0, 0)
			mixer:Dock(TOP)
			local palette = vgui.Create("DColorPalette", panel)
			local colors = {}
			for _, color in pairs(colors) do
				colors[#colors+1] = Color(unpack(color))
			end
			palette:SetColorButtons(colors)
			palette.OnValueChanged = function(_, color)
				mixer:SetColor(color)
			end
			palette:DockMargin(0, 3, 0, 0)
			palette:Dock(TOP)
			
			local cluster_1 = vgui.Create("DPanel", panel)
			cluster_1:SetPaintBackground(false)
			cluster_1:DockMargin(0, 3, 0, 0)
			cluster_1:Dock(TOP)
			
			local players_checkbox = vgui.Create("DCheckBoxLabel", cluster_1)
			players_checkbox:SetValue(show_players)
			players_checkbox:SetText("Show players")
			players_checkbox:SetDark(1)
			players_checkbox.OnChange = function(_, state)
				show_players = state
			end
			players_checkbox:DockMargin(0, 0, 10, 0)
			players_checkbox:Dock(LEFT)
			
			local entities_checkbox = vgui.Create("DCheckBoxLabel", cluster_1)
			entities_checkbox:SetValue(show_entities)
			entities_checkbox:SetText("Show entities")
			entities_checkbox:SetDark(1)
			entities_checkbox.OnChange = function(_, state)
				show_entities = state
			end
			entities_checkbox:DockMargin(0, 0, 10, 0)
			entities_checkbox:Dock(LEFT)
			
			local lighting_checkbox = vgui.Create("DCheckBoxLabel", cluster_1)
			lighting_checkbox:SetValue(suppress_lighting)
			lighting_checkbox:SetText("Suppress lighting")
			lighting_checkbox:SetDark(1)
			lighting_checkbox.OnChange = function(_, state)
				suppress_lighting = state
			end
			lighting_checkbox:DockMargin(0, 0, 3, 0)
			lighting_checkbox:Dock(LEFT)
			
			local always_show_checkbox = vgui.Create("DCheckBoxLabel", panel)
			always_show_checkbox:SetValue(not always_show)
			always_show_checkbox:SetText("Only show through walls")
			always_show_checkbox:SetTooltip(
				"If checked, chams will only render through walls.\n"..
				"May reduce framerate."
			)
			always_show_checkbox:SetDark(1)
			always_show_checkbox.OnChange = function(_, state)
				always_show = not state
			end
			always_show_checkbox:DockMargin(0, 0, 3, 0)
			always_show_checkbox:Dock(TOP)
			
			local range_checkbox = vgui.Create("DCheckBoxLabel", panel)
			range_checkbox:SetValue(range_enabled)
			range_checkbox:SetText("Enforce maximum range")
			range_checkbox:SetTooltip(
				"If checked, chams won't be rendered for players or entities beyond the selected range.\n"..
				"Useful for servers with large numbers of players or entities."
			)
			range_checkbox:SetDark(1)
			range_checkbox.OnChange = function(_, state)
				range_enabled = state
			end
			range_checkbox:DockMargin(0, 0, 3, 0)
			range_checkbox:Dock(TOP)
			
			local range_slider = vgui.Create("DNumSlider", panel)
			range_slider:SetMin(0)
			range_slider:SetMax(32768)
			range_slider:SetDecimals(0)
			range_slider:SetValue(range)
			range_slider:SetDark(1)
			range_slider:SetText("Maximum range")
			range_slider.OnValueChanged = function(_, value)
				range = value
			end
			range_slider:DockMargin(3, 0, 3, 0)
			range_slider:Dock(TOP)
			
			local container_add_text = vgui.Create("DPanel", panel)
			container_add_text:SetDrawBackground(false)
			container_add_text:DockMargin(0, 3, 0, 0)
			container_add_text:Dock(TOP)
			local add_text_label = vgui.Create("DLabel", container_add_text)
			add_text_label:SetText("Add pattern")
			add_text_label:SetDark(1)
			add_text_label:SetContentAlignment(6)
			add_text_label:Dock(LEFT)
			local add_text = vgui.Create("DTextEntry", container_add_text)
			add_text.OnValueChange = function()
				classes[add_text:GetValue()] = mixer:GetColor()
			end
			add_text:SetTooltip("Adds a Lua pattern (similar to a regular expression) to be targeted.")
			add_text:DockMargin(3, 0, 3, 0)
			add_text:Dock(FILL)
			local add_text_button = vgui.Create("DImageButton", container_add_text)
			add_text_button.DoClick = add_text.OnValueChange
			add_text_button:SetImage("icon16/add.png")
			add_text_button:SetStretchToFit(false)
			add_text_button:SetWide(16)
			add_text_button:Dock(RIGHT)
			
			local container_add_box = vgui.Create("DPanel", panel)
			container_add_box:SetDrawBackground(false)
			container_add_box:DockMargin(0, 3, 0, 0)
			container_add_box:Dock(TOP)
			local add_box_label = vgui.Create("DLabel", container_add_box)
			add_box_label:SetText("Add entity")
			add_box_label:SetDark(1)
			add_box_label:SetContentAlignment(6)
			add_box_label:Dock(LEFT)
			local add_box = vgui.Create("DComboBox", container_add_box)
			add_box.refresh = function()
				add_box:Clear()
				local entities = ents.GetAll()
				local classes = {}
				for k, v in pairs(entities) do
					classes[v:GetClass()] = true
				end
				for class in pairs(classes) do
					add_box:AddChoice(tostring(class))
				end
				add_box:SetValue("Select an entity...")
			end
			add_box.DoRightClick = add_box.refresh
			add_box.refresh()
			add_box:DockMargin(3, 0, 3, 0)
			add_box:Dock(FILL)
			local add_box_button = vgui.Create("DImageButton", container_add_box)
			add_box_button.DoClick = function()
				local text, data = add_box:GetSelected()
				if text then
					text = string.gsub(text, "[%.%+%-%*%?%(%)%[%]%%]", {
						["."] = "%.",
						["+"] = "%+",
						["-"] = "%-",
						["*"] = "%*",
						["?"] = "%?",
						["("] = "%(",
						[")"] = "%)",
						["["] = "%[",
						["]"] = "%]",
						["%"] = "%%"
					})
					classes["^"..text.."$"] = mixer:GetColor()
				end
			end
			add_box_button:SetImage("icon16/add.png")
			add_box_button:SetStretchToFit(false)
			add_box_button:SetWide(16)
			add_box_button:Dock(RIGHT)
			local container_remove = vgui.Create("DPanel", panel)
			container_remove:SetDrawBackground(false)
			container_remove:DockMargin(0, 3, 0, 0)
			container_remove:Dock(TOP)
			local remove_label = vgui.Create("DLabel", container_remove)
			remove_label:SetText("Remove entity")
			remove_label:SetDark(1)
			remove_label:SetContentAlignment(6)
			remove_label:Dock(LEFT)
			local remove_entry = vgui.Create("DComboBox", container_remove)
			remove_entry.refresh = function()
				remove_entry:Clear()
				for ent in pairs(classes) do
					remove_entry:AddChoice(tostring(ent), ent)
				end
				remove_entry:SetValue("Select an entity...")
			end
			remove_entry.DoRightClick = remove_entry.refresh
			remove_entry.refresh()
			remove_entry:DockMargin(3, 0, 3, 0)
			remove_entry:Dock(FILL)
			local remove_button = vgui.Create("DImageButton", container_remove)
			remove_button:SetImage("icon16/delete.png")
			remove_button:SetStretchToFit(false)
			remove_button:SetWide(16)
			remove_button.DoClick = function()
				local _, data = remove_entry:GetSelected()
				if data then classes[data] = nil end
			end
			remove_button:Dock(RIGHT)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("RenderScreenspaceEffects", "Chams")
	end
}
