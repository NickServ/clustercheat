--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]

-- This code is an absolute fucking mess
--
--
--
--
-- but it works.

return {
	name = "Auto-collector",
	description = "Automatically collects loose items.",
	license = "CC0 1.0 Universal",
	icon = "icon16/mouse.png",
	start = function(filename, panel)
		local glon = real_g.glon
		if not glon then
			print("Missing glon. Quitting!")
			return
		end
		
		local enabled = false
		local verbose = false
		local draw_bounding_boxes = false
		
		local classes = {
			"_item_sent$"
		}
		
		-- So that it won't spam console
		local collected = {}
		
		local old_print = print
		local print = function(...)
			if verbose then
				old_print(...)
			end
		end
		
		local set_noclip = function(bool)
			--print("Setting noclip to "..tostring(bool))
			local noclip = LocalPlayer():GetMoveType() == MOVETYPE_NOCLIP
			if bool ~= noclip then
				RunConsoleCommand("noclip")
				noclip = bool
			end
		end
		local moving = false
		local set_moving = function(bool)
			--print("Setting moving to "..tostring(bool))
			if bool ~= moving then
				if bool then
					RunConsoleCommand("+forward")
				else
					RunConsoleCommand("-forward")
				end
				moving = bool
			end
		end
		local mode
		local candidate
		local candidate_pos
		local candidate_class
		local distance
		hook_add("Think", "Autocollect", function()
			if enabled then
				local distance_from_candidate
				local user_pos = LocalPlayer():GetShootPos()
				for _, ent in pairs(ents.GetAll()) do
					local found = false
					if IsValid(ent) then
						if ent:IsPlayer() then continue end
						if not ent:IsSolid() then continue end
						local matched = false
						for _, class in pairs(classes) do
							if string.match(ent:GetClass(), class) then
								matched = true
								break
							end
						end
						if not matched then continue end
						
						local distance_from_ent = user_pos:DistToSqr(ent:GetPos())
						if (not distance_from_candidate) or (distance_from_ent < distance_from_candidate) then
							found = true
							candidate = ent
							candidate_pos = ent:GetPos()
							candidate_class = ent:GetClass()
							distance_from_candidate = distance_from_ent
							distance = distance_from_candidate
						end
					end
					if not found then
						distance_from_candidate = nil
					end
				end
				if candidate_pos then
					distance = user_pos:DistToSqr(candidate_pos)
					local near = distance < 100
					if near and not IsValid(candidate) then
						candidate_pos = nil
						return
					end
					set_noclip(true)
					if near then
						set_moving(false)
						if not collected[candidate:EntIndex()] then
							mode = "Collecting item"
							RunConsoleCommand("_itmc", candidate:EntIndex(), glon.encode({"__options", "Add to Backpack", "__backpack", "start"}))
							--RunConsoleCommand("_itmc", candidate:EntIndex(), glon.encode({"__options", "__backpack", "end"}))
							collected[candidate:EntIndex()] = true
						else
							mode = "Collected"
						end
					else
						set_moving(true)
						mode = "Going to item"
					end
					LocalPlayer():SetEyeAngles( (candidate_pos - user_pos):Angle() )
				else
					mode = "Idle"
					set_moving(false)
					--set_noclip(false)
				end
			end
		end)
		local movetype = {
			"Isometric",
			"Walk",
			"Step",
			"Fly (No gravity)",
			"Fly (Gravity)",
			"Physics",
			"Push",
			"Noclip",
			"Ladder",
			"Spectator",
			"Custom"
		}
		movetype[0] = "None"
		hook_add("HUDPaint", "Autocollect", function()
			local text = {
				enabled and "Enabled" or "Disabled",
				mode,
				candidate_class or "nil",
				movetype[LocalPlayer():GetMoveType()],
				(distance and math.floor(distance)) or "nil"
			}
			for i=1, #text do
				local font
				if i==1 then
					font = "TargetID"
				else
					font = "TargetIDSmall"
				end
				draw.SimpleTextOutlined(
					text[i],
					font,
					ScrW()/2,
					ScrH()/2+12*i,
					Color(255, 255, 255, 255),
					TEXT_ALIGN_CENTER,
					TEXT_ALIGN_TOP,
					0,
					Color(0, 0, 0, 255)
				)
			end
			if draw_bounding_boxes then
				cam.Start3D()
					if IsValid(candidate) then
						local minimum, maximum = candidate:GetModelBounds()
						render.DrawWireframeBox(candidate:GetPos(), candidate:GetAngles(), minimum, maximum)
					end
				cam.End3D()
			end
		end)
		if panel then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			local verbose_checkbox = vgui.Create("DCheckBoxLabel", panel)
			verbose_checkbox:SetValue(verbose)
			verbose_checkbox:SetText("Verbose")
			verbose_checkbox:SetDark(1)
			verbose_checkbox.OnChange = function(_, state)
				verbose = state
			end
			verbose_checkbox:DockMargin(0, 3, 0, 0)
			verbose_checkbox:Dock(TOP)
			
			local boxes_checkbox = vgui.Create("DCheckBoxLabel", panel)
			boxes_checkbox:SetValue(draw_bounding_boxes)
			boxes_checkbox:SetText("Draw bounding boxes")
			boxes_checkbox:SetDark(1)
			boxes_checkbox.OnChange = function(_, state)
				draw_bounding_boxes = state
			end
			boxes_checkbox:DockMargin(0, 3, 0, 0)
			boxes_checkbox:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("Think", "Autocollect")
		hook_remove("HUDPaint", "Autocollect")
		RunConsoleCommand("-forward")
	end
}
