--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
-- Intended to be more of a debugging/development tool than a cheat tool
return {
	name = "What am I looking at?",
	description = "Displays information about players and entities.",
	license = "CC0 1.0 Universal",
	icon = "icon16/text_align_center.png",
	start = function(filename, panel)
		local order = {
			"Entity",
			"HitPos",
			"HitTexture",
			"MatType",
			"SurfaceFlags",
			"DispFlags"
		}
		local range = 1000
		hook_add("HUDPaint", "WAILA", function()
			-- Now uses Global shit instead of Player shit, so it should
			-- work with GM:CalcView (which means FREECAM SUPPORT!)
			local endpos = EyeVector()*range
			local trace = util.QuickTrace(EyePos(), endpos, LocalPlayer())
			if trace.Hit then
				local text = {}
				if trace.Entity then
					if trace.Entity:IsPlayer() then
						text[1] = trace.Entity:Nick()
						text[2] = "SteamID: "..trace.Entity:SteamID()
						text[3] = "Health: "..trace.Entity:Health()
						text[4] = "Armor: "..trace.Entity:Armor()
						text[5] = "Team: "..trace.Entity:Team().." ("..team.GetName(trace.Entity:Team())..")"
						text[6] = "Usergroup: "..trace.Entity:GetUserGroup()
						text[7] = "----------------"
					else
						text[1] = trace.Entity:GetClass()
						text[2] = "Model: "..trace.Entity:GetModel()
						text[3] = "Position: "..tostring(trace.Entity:GetPos())
						text[4] = "Velocity: "..tostring(trace.Entity:GetVelocity())
						text[5] = "Health: "..trace.Entity:Health()
						text[6] = "Water level: "..trace.Entity:WaterLevel()
						text[7] = "Flags: "..trace.Entity:GetFlags()
						text[8] = "----------------"
					end
					for i=1, #order do
						if trace[order[i]] then
							text[#text+1] = order[i]..": "..tostring(trace[order[i]])
						end
					end
				elseif trace.EndPos == endpos then
					text[1] = "Out of range"
				else
					text[1] = "???"
				end
				for i=1, #text do
					local font
					if i==1 then
						font = "TargetID"
					else
						font = "TargetIDSmall"
					end
					draw.SimpleTextOutlined(
						text[i],
						font,
						ScrW()/2,
						ScrH()/2+12*i,
						Color(255, 255, 255, 255),
						TEXT_ALIGN_CENTER,
						TEXT_ALIGN_TOP,
						0,
						Color(0, 0, 0, 255)
					)
				end
			end
		end)
		if panel and panel.IsValid and panel:IsValid() then
			local range_slider = vgui.Create("DNumSlider", panel)
			range_slider:SetMin(0)
			range_slider:SetMax(32768)
			range_slider:SetDecimals(1)
			range_slider:SetValue(range)
			range_slider:SetDark(1)
			range_slider:SetText("Maximum range")
			range_slider.OnValueChanged = function(_, value)
				range = value
			end
			range_slider:DockMargin(3, 0, 3, 0)
			range_slider:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("HUDPaint", "WAILA")
	end
}
