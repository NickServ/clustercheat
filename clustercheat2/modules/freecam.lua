--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Freecam",
	description = "Allows you to move the camera freely.",
	license = "CC0 1.0 Universal",
	icon = "icon16/camera_go.png",
	start = function(filename, panel)
		local enabled = false
		local speed = 7
		local sens = 0.05
		local ply = LocalPlayer()
		local pos = ply:EyePos() or Vector()
		local look = ply:EyeAngles() or Angle()
		local vel = Vector()
		hook_add("CreateMove", "Freecam", function(cmd)
			if enabled then
				-- Movement
				local vel_temp = Vector()
				if cmd:KeyDown(IN_FORWARD) then vel_temp = vel_temp + look:Forward() end
				if cmd:KeyDown(IN_BACK) then vel_temp = vel_temp - look:Forward() end
				if cmd:KeyDown(IN_MOVELEFT) then vel_temp = vel_temp - look:Right() end
				if cmd:KeyDown(IN_MOVERIGHT) then vel_temp = vel_temp + look:Right() end
				if cmd:KeyDown(IN_JUMP) then vel_temp = vel_temp + look:Up() end
				if cmd:KeyDown(IN_DUCK) then vel_temp = vel_temp - look:Up() end
				local speed = speed
				if cmd:KeyDown(IN_SPEED) then speed = speed * 4 end
				if cmd:KeyDown(IN_WALK) then speed = speed / 4 end
				pos = pos + (vel + (vel_temp * speed))
				
				-- Look
				look.yaw = look.yaw - (cmd:GetMouseX() * sens)
				look.pitch = look.pitch + (cmd:GetMouseY() * sens)
				
				-- No matter what I do I can't stop my character from
				-- turning... Please help
				cmd:ClearButtons()
				cmd:ClearMovement()
				cmd:SetButtons(0)
				cmd:SetForwardMove(0)
				cmd:SetMouseWheel(0)
				cmd:SetMouseX(0)
				cmd:SetMouseY(0)
				cmd:SetSideMove(0)
				cmd:SetUpMove(0)
				return true
			end
		end)
		hook_add("CalcView", "Freecam", function(ply, origin, angles, fov, znear, zfar)
			if enabled then
				return {
					origin = pos,
					angles = look,
					fov = fov,
					znear = znear,
					zfar = zfar,
					drawviewer = true
				}
			else
				return {
					origin = origin,
					angles = angles,
					fov = fov,
					znear = znear,
					zfar = zfar,
					drawviewer = false
				}
			end
		end)
		if panel and panel.IsValid and panel:IsValid() then
			local cluster = vgui.Create("DPanel", panel)
			cluster:SetPaintBackground(false)
			cluster:DockMargin(0, 3, 0, 0)
			cluster:Dock(TOP)
			
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", cluster)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			--enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(LEFT)
			
			local reset_button = vgui.Create("DButton", cluster)
			reset_button:SetText("Reset position and angle")
			reset_button:DockMargin(3, 0, 0, 0)
			reset_button.DoClick = function()
				pos = ply:EyePos() or Vector()
				look = ply:EyeAngles() or Angle()
			end
			reset_button:Dock(FILL)
			
			local speed_slider = vgui.Create("DNumSlider", panel)
			speed_slider:SetMin(0)
			speed_slider:SetMax(100)
			speed_slider:SetDecimals(1)
			speed_slider:SetValue(speed)
			speed_slider:SetDark(1)
			speed_slider:SetText("Speed")
			speed_slider.OnValueChanged = function(_, value)
				speed = value
			end
			speed_slider:DockMargin(3, 0, 3, 0)
			speed_slider:Dock(TOP)
			
			local sens_slider = vgui.Create("DNumSlider", panel)
			sens_slider:SetMin(0)
			sens_slider:SetMax(0.5)
			sens_slider:SetDecimals(2)
			sens_slider:SetValue(sens)
			sens_slider:SetDark(1)
			sens_slider:SetText("Mouse sensitivity")
			sens_slider.OnValueChanged = function(_, value)
				sens = value
			end
			sens_slider:DockMargin(3, 0, 3, 0)
			sens_slider:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("CreateMove", "Freecam")
		hook_remove("CalcView", "Freecam")
	end
}
