--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Anti-aim",
	description = "Fakes your viewangles.",
	license = "CC0 1.0 Universal",
	--icon = "icon16/sport_shuttlecock.png",
	start = function(filename, panel)
		local enabled = false
		local pretty_viewangle = LocalPlayer():EyeAngles()
		local ugly_viewangle = Angle()
		local pretty = false
		local sensitivity = 0.05
		hook_add("CreateMove", "antiaim", function(cucmd)
			pretty_viewangle.pitch = pretty_viewangle.pitch + (cucmd:GetMouseY()*sensitivity)
			if pretty_viewangle.pitch < -89 then
				pretty_viewangle.pitch = -89
			elseif pretty_viewangle.pitch > 89 then
				pretty_viewangle.pitch = 89
			end
			
			pretty_viewangle.yaw = pretty_viewangle.yaw - (cucmd:GetMouseX()*sensitivity)
			if pretty_viewangle.yaw < -179 then
				pretty_viewangle.yaw = 179
			elseif pretty_viewangle.yaw > 179 then
				pretty_viewangle.yaw = -179
			end
			
			pretty_viewangle.roll = 0
			
			if enabled then
				pretty = false
				if cucmd:KeyDown(IN_ATTACK) then
					cucmd:SetViewAngles(pretty_viewangle)
				else
					ugly_viewangle = Angle(181, pretty_viewangle.yaw, 180)
					cucmd:SetViewAngles(ugly_viewangle)
				end
			elseif pretty then
				cucmd:SetViewAngles(pretty_viewangle)
				pretty = true
			end
		end)
		hook_add("CalcView", "antiaim", function(ply, origin, angles, fov, znear, zfar)
			return {
				ply = ply,
				origin = origin,
				angles = pretty_viewangle or angles,
				fov = fov,
				znear = znear,
				zfar = zfar,
				false,
				nil
			}
		end)
		if panel and panel:IsValid() then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			local sens_slider = vgui.Create("DNumSlider", panel)
			sens_slider:SetMin(0)
			sens_slider:SetMax(0.5)
			sens_slider:SetDecimals(2)
			sens_slider:SetValue(sensitivity)
			sens_slider:SetDark(1)
			sens_slider:SetText("Mouse sensitivity")
			sens_slider.OnValueChanged = function(_, value)
				sensitivity = value
			end
			sens_slider:DockMargin(3, 0, 3, 0)
			sens_slider:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("CreateMove", "antiaim")
		hook_remove("CalcView", "antiaim")
		LocalPlayer():SetEyeAngles(Angle()) -- important
	end
}
