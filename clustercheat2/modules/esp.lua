--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Extrasensory perception",
	description = "Displays information about players.",
	license = "CC0 1.0 Universal",
	icon = "icon16/vcard.png",
	start = function(filename, panel)
		local enabled = true
		local range_enabled = false
		local range = 2000
		local conf = {
			box2d = true,
			box3d = false,
			health = true,
			armor = true,
			name = true,
			steamid = false,
			flavor = true,
			team = true,
			group = true
		}
		local bar_width = 10
		local spacing = 3
		
		local file = file.Open(path.."/esp_text.json", "r", "LUA")
		local file_contents = file:Read(file:Size())
		file:Close()
		local flavor_text = util.JSONToTable(file_contents) or {}
		
		hook_add("PostDrawHUD", "ESP", function()
			if enabled then
				local players = player.GetAll()
				for _, ply in pairs(players) do
					if ply ~= LocalPlayer() and ply:IsValid() and ply:Alive() then
						local distance = EyePos():DistToSqr(ply:GetPos())
						if range_enabled then
							if distance > range*range then continue end
						end
						local minimum, maximum = ply:GetCollisionBounds()
						local pos = ply:GetPos()
						local color = team.GetColor(ply:Team()) or Color(unpack(colors.primary))
						if conf.box3d then
							cam.Start3D()
							render.DrawWireframeBox(pos, Angle(), minimum, maximum, color)
							cam.End3D()
						end
						--[[
							Advice: When trying to figure this kind of
							thing out, write it down on paper and try to
							visualize it.
						]]
						minimum:Add(pos)
						maximum:Add(pos)
						local corners = {
							minimum,
							Vector(minimum.x, maximum.y, minimum.z),
							Vector(maximum.x, minimum.y, minimum.z),
							Vector(maximum.x, maximum.y, minimum.z),
							Vector(minimum.x, minimum.y, maximum.z),
							Vector(minimum.x, maximum.y, maximum.z),
							Vector(maximum.x, minimum.y, maximum.z),
							maximum
						}
						local smallest_x = math.min(
							corners[1]:ToScreen().x,
							corners[2]:ToScreen().x,
							corners[3]:ToScreen().x,
							corners[4]:ToScreen().x,
							corners[5]:ToScreen().x,
							corners[6]:ToScreen().x,
							corners[7]:ToScreen().x,
							corners[8]:ToScreen().x
						)
						local smallest_y = math.min(
							corners[1]:ToScreen().y,
							corners[2]:ToScreen().y,
							corners[3]:ToScreen().y,
							corners[4]:ToScreen().y,
							corners[5]:ToScreen().y,
							corners[6]:ToScreen().y,
							corners[7]:ToScreen().y,
							corners[8]:ToScreen().y
						)
						local biggest_x = math.max(
							corners[1]:ToScreen().x,
							corners[2]:ToScreen().x,
							corners[3]:ToScreen().x,
							corners[4]:ToScreen().x,
							corners[5]:ToScreen().x,
							corners[6]:ToScreen().x,
							corners[7]:ToScreen().x,
							corners[8]:ToScreen().x
						)
						local biggest_y = math.max(
							corners[1]:ToScreen().y,
							corners[2]:ToScreen().y,
							corners[3]:ToScreen().y,
							corners[4]:ToScreen().y,
							corners[5]:ToScreen().y,
							corners[6]:ToScreen().y,
							corners[7]:ToScreen().y,
							corners[8]:ToScreen().y
						)
						if conf.box2d then
							surface.SetDrawColor(Color(unpack(colors.black)))
							surface.DrawOutlinedRect(
								smallest_x,
								smallest_y,
								biggest_x-smallest_x,
								biggest_y-smallest_y
							)
							surface.DrawOutlinedRect(
								smallest_x+2,
								smallest_y+2,
								biggest_x-smallest_x-4,
								biggest_y-smallest_y-4
							)
							surface.SetDrawColor(color)
							surface.DrawOutlinedRect(
								smallest_x+1,
								smallest_y+1,
								biggest_x-smallest_x-2,
								biggest_y-smallest_y-2
							)
							surface.SetDrawColor(0, 0, 0, 0)
						end
						
						local bars = 0
						local bar_width_calculated
						if conf.health then
							bars = bars + 1
							local health = math.Clamp(ply:Health() or 0, 0, 100)
							bar_width_calculated = math.Clamp(math.ceil( bar_width/((distance+562500)/562500)), 3, bar_width )
							
							surface.SetDrawColor(Color(unpack(colors.black)))
							surface.DrawOutlinedRect(
								smallest_x-((bar_width_calculated+spacing)*bars),
								smallest_y,
								bar_width_calculated,
								biggest_y-smallest_y
							)
							
							surface.SetDrawColor(Color(unpack(colors.success)))
							surface.DrawRect(
								smallest_x-((bar_width_calculated+spacing)*bars)+1,
								smallest_y+1,
								bar_width_calculated-2,
								(biggest_y-smallest_y-2)*(health/100)
							)
							
							surface.SetDrawColor(0, 0, 0, 0)
						end
						if conf.armor then
							bars = bars + 1
							local armor = math.Clamp(ply:Armor() or 0, 0, 100)
							bar_width_calculated = bar_width_calculated or math.Clamp(math.ceil( bar_width/((distance+562500)/562500)), 3, bar_width )
							
							surface.SetDrawColor(Color(unpack(colors.black)))
							surface.DrawOutlinedRect(
								smallest_x-((bar_width_calculated+spacing)*bars),
								smallest_y,
								bar_width_calculated,
								biggest_y-smallest_y
							)
							
							surface.SetDrawColor(Color(unpack(colors.info)))
							surface.DrawRect(
								smallest_x-((bar_width_calculated+spacing)*bars)+1,
								smallest_y+1,
								bar_width_calculated-2,
								(biggest_y-smallest_y-2)*(armor/100)
							)
							
							surface.SetDrawColor(0, 0, 0, 0)
						end
						
						local text = {}
						if conf.name then
							text[#text+1] = ply:Nick()
						end
						if conf.steamid then
							text[#text+1] = ply:SteamID()
						end
						if conf.flavor then
							text[#text+1] = flavor_text[ply:SteamID()]
						end
						if conf.team then
							text[#text+1] = team.GetName(ply:Team())
						end
						if conf.group then
							text[#text+1] = ply:GetUserGroup()
						end
						for i=1, #text do
							local font
							if i==1 then
								font = "TargetID"
							else
								font = "TargetIDSmall"
							end
							draw.SimpleTextOutlined(
								text[i],
								font,
								biggest_x+spacing,
								smallest_y+(12*i)-12,
								Color(255, 255, 255, 255),
								TEXT_ALIGN_LEFT,
								TEXT_ALIGN_TOP,
								0,
								Color(0, 0, 0, 255)
							)
						end
					end
				end
			end
		end)
		if panel and panel.IsValid and panel:IsValid() then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, value)
				enabled = value
			end
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			local range_checkbox = vgui.Create("DCheckBoxLabel", panel)
			range_checkbox:SetValue(range_enabled)
			range_checkbox:SetText("Enforce maximum range")
			range_checkbox:SetTooltip(
				"If checked, ESP info won't be rendered for players beyond the selected range.\n"..
				"Useful for servers with large numbers of players."
			)
			range_checkbox:SetDark(1)
			range_checkbox.OnChange = function(_, state)
				range_enabled = state
			end
			range_checkbox:DockMargin(0, 3, 0, 0)
			range_checkbox:Dock(TOP)
			local range_slider = vgui.Create("DNumSlider", panel)
			range_slider:SetMin(0)
			range_slider:SetMax(32768)
			range_slider:SetDecimals(0)
			range_slider:SetValue(range)
			range_slider:SetDark(1)
			range_slider:SetText("Maximum range")
			range_slider.OnValueChanged = function(_, value)
				range = value
			end
			range_slider:DockMargin(0, 3, 0, 0)
			range_slider:Dock(TOP)
			
			local boxes = {
				box2d = "Show 2D bounding boxes",
				box3d = "Show 3D bounding boxes",
				health = "Show health",
				armor = "Show armor",
				name = "Show display name",
				steamid = "Show SteamID",
				flavor = "Show flavor text", -- could use better name
				team = "Show team",
				group = "Show usergroup"
			}
			local boxes_order = {
				"box2d",
				"box3d",
				"health",
				"armor",
				"name",
				"steamid",
				"flavor",
				"team",
				"group"
			}
			for i=1, #boxes_order do
				local checkbox = vgui.Create("DCheckBoxLabel", panel)
				checkbox:SetText(boxes[boxes_order[i]])
				checkbox:SetDark(1)
				checkbox.OnChange = function(_, value)
					conf[boxes_order[i]] = value
				end
				checkbox:SetValue(conf[boxes_order[i]])
				checkbox:DockMargin(0, 3, 0, 0)
				checkbox:Dock(TOP)
			end
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("PostDrawHUD", "ESP")
	end
}
