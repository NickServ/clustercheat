--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Flashlight spam",
	description = "Noisily spams your flashlight.",
	license = "CC0 1.0 Universal",
	icon = "icon16/lightbulb.png",
	start = function(filename, panel)
		local enabled = false
		hook_add("Think", "lightspam", function()
			if enabled then RunConsoleCommand("impulse", "100") end
		end)
		-- TIL that panel.IsValid() with a period instead of a colon
		-- causes the script to hang without creating an error
		if panel and panel:IsValid() then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		else
			command_add("+lightspam", function()
				enabled = true
			end, nil, "Enables flashlight spam.")
			command_add("-lightspam", function()
				enabled = false
			end, nil, "Disables flashlight spam.")
		end
	end,
	stop = function()
		hook_remove("Think", "lightspam")
	end
}
