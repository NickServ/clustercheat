--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Aimbot",
	description = "Automatically aims at players.",
	license = "CC0 1.0 Universal",
	icon = "icon16/mouse.png",
	start = function(filename, panel)
		local enabled = true
		local range = 1000*1000
		local bone = "ValveBiped.Bip01_Head1"
		local only_when_shooting = false
		local only_target_solids = true
		local target_players = true
		local target_entities = false
		local players = {}
		local classes = {}
		hook_add("CreateMove", "Aimbot", function(cucmd)
			if enabled and (target_players or target_entities) and not (only_when_shooting and not cucmd:KeyDown(IN_ATTACK)) then
				local candidate
				local distance_from_candidate = range
				local user_pos = LocalPlayer():GetShootPos()
				for _, ent in pairs(ents.FindInSphere(user_pos, range)) do
					if IsValid(ent) and ent ~= LocalPlayer() then
						local found = false
						if target_players and ent:IsPlayer() then
							found = true
							if not ent:Alive() then continue end
							if next(players) and not players[ent:SteamID()] then continue end
						end
						if target_entities and (not ent:IsPlayer()) then
							found = true
							local matches = false
							for class, _ in pairs(classes) do
								if string.match(tostring(ent:GetClass()), class) then
									matches = true
									break
								end
							end
							if not matches then continue end
						end
						if not found then continue end
						if only_target_solids and not ent:IsSolid() then continue end
						
						local ent_pos
						if ent:IsPlayer() then
							ent_pos = ent:GetBonePosition(ent:LookupBone(bone or "ValveBiped.Bip01_Head1") or 6) or (ent.EyePos and ent:EyePos()) or ent:GetPos()
						else
							local minimum, maximum = ent:GetCollisionBounds()
							ent_pos = LerpVector(0.5, minimum, maximum)
							ent_pos:Rotate(ent:GetAngles())
							ent_pos:Add(ent:GetPos())
						end
						
						local distance_from_ent = user_pos:DistToSqr(ent_pos)
						if distance_from_ent < distance_from_candidate then
							candidate = ent_pos
							distance_from_candidate = distance_from_ent
						end
					end
				end
				if candidate then
					cucmd:SetViewAngles( ((candidate-user_pos):Angle()) )
				end
			end
		end)
		if panel then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			local only_when_shooting_checkbox = vgui.Create("DCheckBoxLabel", panel)
			only_when_shooting_checkbox:SetValue(only_when_shooting)
			only_when_shooting_checkbox:SetText("Only target when attacking")
			only_when_shooting_checkbox:SetDark(1)
			only_when_shooting_checkbox.OnChange = function(_, state)
				only_when_shooting = state
			end
			only_when_shooting_checkbox:DockMargin(0, 3, 0, 0)
			only_when_shooting_checkbox:Dock(TOP)
			
			local range_slider = vgui.Create("DNumSlider", panel)
			range_slider:SetMin(0)
			range_slider:SetMax(32768)
			range_slider:SetDecimals(0)
			range_slider:SetValue(math.sqrt(range))
			range_slider:SetDark(1)
			range_slider:SetText("Maximum range")
			range_slider.OnValueChanged = function(_, value)
				range = value^2
			end
			range_slider:DockMargin(3, 0, 3, 0)
			range_slider:Dock(TOP)
			
			local container_bone = vgui.Create("DPanel", panel)
			container_bone:SetDrawBackground(false)
			container_bone:DockMargin(0, 3, 0, 0)
			container_bone:Dock(TOP)
			local bone_label = vgui.Create("DLabel", container_bone)
			bone_label:SetText("Bone")
			bone_label:SetDark(1)
			bone_label:SetContentAlignment(6)
			bone_label:Dock(LEFT)
			local bone_box = vgui.Create("DComboBox", container_bone)
			bone_box:AddChoice("Head", "ValveBiped.Bip01_Head1")
			bone_box:AddChoice("Neck", "ValveBiped.Bip01_Neck")
			bone_box:AddChoice("Spine 1", "ValveBiped.Bip01_Spine1")
			bone_box:AddChoice("Spine 2", "ValveBiped.Bip01_Spine2")
			bone_box:AddChoice("Spine 3", "ValveBiped.Bip01_Spine3")
			bone_box:AddChoice("Left clavicle", "ValveBiped.Bip01_L_Clavicle")
			bone_box:AddChoice("Right clavicle", "ValveBiped.Bip01_R_Clavicle")
			bone_box:AddChoice("Left arm", "ValveBiped.Bip01_L_UpperArm")
			bone_box:AddChoice("Right arm", "ValveBiped.Bip01_R_UpperArm")
			bone_box:AddChoice("Left forearm", "ValveBiped.Bip01_L_Forearm")
			bone_box:AddChoice("Right forearm", "ValveBiped.Bip01_R_Forearm")
			bone_box:AddChoice("Left hand", "ValveBiped.Bip01_L_Hand")
			bone_box:AddChoice("Right hand", "ValveBiped.Bip01_R_Hand")
			bone_box:AddChoice("Pelvis", "ValveBiped.Bip01_Pelvis")
			bone_box:AddChoice("Left thigh", "ValveBiped.Bip01_L_Thigh")
			bone_box:AddChoice("Right thigh", "ValveBiped.Bip01_R_Thigh")
			bone_box:AddChoice("Left calf", "ValveBiped.Bip01_L_Calf")
			bone_box:AddChoice("Right calf", "ValveBiped.Bip01_R_Calf")
			bone_box:AddChoice("Left foot", "ValveBiped.Bip01_L_Foot")
			bone_box:AddChoice("Right foot", "ValveBiped.Bip01_R_Foot")
			bone_box:SetValue("Head")
			bone_box.OnSelect = function(_, _, _, data)
				bone = data or bone
			end
			bone_box:DockMargin(3, 0, 3, 0)
			bone_box:Dock(FILL)
			
			do -- players
				local players_checkbox = vgui.Create("DCheckBoxLabel", panel)
				players_checkbox:SetValue(target_players)
				players_checkbox:SetText("Target players")
				players_checkbox:SetDark(1)
				players_checkbox.OnChange = function(_, state)
					target_players = state
				end
				players_checkbox:DockMargin(0, 3, 0, 0)
				players_checkbox:Dock(TOP)
				
				local container_add_text = vgui.Create("DPanel", panel)
				container_add_text:SetDrawBackground(false)
				container_add_text:DockMargin(0, 3, 0, 0)
				container_add_text:Dock(TOP)
				local add_text_label = vgui.Create("DLabel", container_add_text)
				add_text_label:SetText("Add pattern")
				add_text_label:SetDark(1)
				add_text_label:SetContentAlignment(6)
				add_text_label:Dock(LEFT)
				local add_text = vgui.Create("DTextEntry", container_add_text)
				add_text.OnValueChange = function()
					classes[add_text:GetValue()] = mixer:GetColor()
				end
				add_text:SetTooltip("Adds a Lua pattern (similar to a regular expression) to be targeted.")
				add_text:DockMargin(3, 0, 3, 0)
				add_text:Dock(FILL)
				local add_text_button = vgui.Create("DImageButton", container_add_text)
				add_text_button.DoClick = add_text.OnValueChange
				add_text_button:SetImage("icon16/add.png")
				add_text_button:SetStretchToFit(false)
				add_text_button:SetWide(16)
				add_text_button:Dock(RIGHT)
				
				local container_add_box = vgui.Create("DPanel", panel)
				container_add_box:SetDrawBackground(false)
				container_add_box:DockMargin(0, 3, 0, 0)
				container_add_box:Dock(TOP)
				local add_box_label = vgui.Create("DLabel", container_add_box)
				add_box_label:SetText("Add player")
				add_box_label:SetDark(1)
				add_box_label:SetContentAlignment(6)
				add_box_label:Dock(LEFT)
				local add_box = vgui.Create("DComboBox", container_add_box)
				add_box.refresh = function()
					add_box:Clear()
					local players = player.GetAll()
					for i=1, #players do
						add_box:AddChoice(players[i]:Nick(), players[i]:SteamID())
					end
					add_box:SetValue("Select a player...")
				end
				add_box.DoRightClick = add_box.refresh
				add_box.refresh()
				add_box:DockMargin(3, 0, 3, 0)
				add_box:Dock(FILL)
				local add_box_button = vgui.Create("DImageButton", container_add_box)
				add_box_button.DoClick = function()
					local text, data = add_box:GetSelected()
					if data then
						players[data] = true
					end
				end
				add_box_button:SetImage("icon16/add.png")
				add_box_button:SetStretchToFit(false)
				add_box_button:SetWide(16)
				add_box_button:Dock(RIGHT)
				local container_remove = vgui.Create("DPanel", panel)
				container_remove:SetDrawBackground(false)
				container_remove:DockMargin(0, 3, 0, 0)
				container_remove:Dock(TOP)
				local remove_label = vgui.Create("DLabel", container_remove)
				remove_label:SetText("Remove player")
				remove_label:SetDark(1)
				remove_label:SetContentAlignment(6)
				remove_label:Dock(LEFT)
				local remove_entry = vgui.Create("DComboBox", container_remove)
				remove_entry.refresh = function()
					remove_entry:Clear()
					for ply in pairs(players) do
						remove_entry:AddChoice(player.GetBySteamID(ply):Nick(), ply)
					end
					remove_entry:SetValue("Select a player...")
				end
				remove_entry.DoRightClick = remove_entry.refresh
				remove_entry.refresh()
				remove_entry:DockMargin(3, 0, 3, 0)
				remove_entry:Dock(FILL)
				local remove_button = vgui.Create("DImageButton", container_remove)
				remove_button:SetImage("icon16/delete.png")
				remove_button:SetStretchToFit(false)
				remove_button:SetWide(16)
				remove_button.DoClick = function()
					local _, data = remove_entry:GetSelected()
					if data then players[data] = nil end
				end
				remove_button:Dock(RIGHT)
			end
			
			do -- entities
				local entities_checkbox = vgui.Create("DCheckBoxLabel", panel)
				entities_checkbox:SetValue(target_entities)
				entities_checkbox:SetText("Target entities")
				entities_checkbox:SetDark(1)
				entities_checkbox.OnChange = function(_, state)
					target_entities = state
				end
				entities_checkbox:DockMargin(0, 3, 0, 0)
				entities_checkbox:Dock(TOP)
				
				local solid_checkbox = vgui.Create("DCheckBoxLabel", panel)
				solid_checkbox:SetValue(only_target_solids)
				solid_checkbox:SetText("Only target solids")
				solid_checkbox:SetDark(1)
				solid_checkbox.OnChange = function(_, state)
					only_target_solids = state
				end
				solid_checkbox:DockMargin(0, 3, 0, 0)
				solid_checkbox:Dock(TOP)
				
				local container_add_text = vgui.Create("DPanel", panel)
				container_add_text:SetDrawBackground(false)
				container_add_text:DockMargin(0, 3, 0, 0)
				container_add_text:Dock(TOP)
				local add_text_label = vgui.Create("DLabel", container_add_text)
				add_text_label:SetText("Add pattern")
				add_text_label:SetDark(1)
				add_text_label:SetContentAlignment(6)
				add_text_label:Dock(LEFT)
				local add_text = vgui.Create("DTextEntry", container_add_text)
				add_text.OnValueChange = function()
					classes[add_text:GetValue()] = true
				end
				add_text:SetTooltip("Adds a Lua pattern (similar to a regular expression) to be targeted.")
				add_text:DockMargin(3, 0, 3, 0)
				add_text:Dock(FILL)
				local add_text_button = vgui.Create("DImageButton", container_add_text)
				add_text_button.DoClick = add_text.OnValueChange
				add_text_button:SetImage("icon16/add.png")
				add_text_button:SetStretchToFit(false)
				add_text_button:SetWide(16)
				add_text_button:Dock(RIGHT)
				
				local container_add_box = vgui.Create("DPanel", panel)
				container_add_box:SetDrawBackground(false)
				container_add_box:DockMargin(0, 3, 0, 0)
				container_add_box:Dock(TOP)
				local add_box_label = vgui.Create("DLabel", container_add_box)
				add_box_label:SetText("Add entity")
				add_box_label:SetDark(1)
				add_box_label:SetContentAlignment(6)
				add_box_label:Dock(LEFT)
				local add_box = vgui.Create("DComboBox", container_add_box)
				add_box.refresh = function()
					add_box:Clear()
					local entities = ents.GetAll()
					local classes = {}
					for k, v in pairs(entities) do
						classes[v:GetClass()] = true
					end
					for class in pairs(classes) do
						add_box:AddChoice(tostring(class))
					end
					add_box:SetValue("Select an entity...")
				end
				add_box.DoRightClick = add_box.refresh
				add_box.refresh()
				add_box:DockMargin(3, 0, 3, 0)
				add_box:Dock(FILL)
				local add_box_button = vgui.Create("DImageButton", container_add_box)
				add_box_button.DoClick = function()
					local text, data = add_box:GetSelected()
					classes["^"..text.."$"] = true
				end
				add_box_button:SetImage("icon16/add.png")
				add_box_button:SetStretchToFit(false)
				add_box_button:SetWide(16)
				add_box_button:Dock(RIGHT)
				local container_remove = vgui.Create("DPanel", panel)
				container_remove:SetDrawBackground(false)
				container_remove:DockMargin(0, 3, 0, 0)
				container_remove:Dock(TOP)
				local remove_label = vgui.Create("DLabel", container_remove)
				remove_label:SetText("Remove entity")
				remove_label:SetDark(1)
				remove_label:SetContentAlignment(6)
				remove_label:Dock(LEFT)
				local remove_entry = vgui.Create("DComboBox", container_remove)
				remove_entry.refresh = function()
					remove_entry:Clear()
					for ent, _ in pairs(classes) do
						remove_entry:AddChoice(tostring(ent), ent)
					end
					remove_entry:SetValue("Select an entity...")
				end
				remove_entry.DoRightClick = remove_entry.refresh
				remove_entry.refresh()
				remove_entry:DockMargin(3, 0, 3, 0)
				remove_entry:Dock(FILL)
				local remove_button = vgui.Create("DImageButton", container_remove)
				remove_button:SetImage("icon16/delete.png")
				remove_button:SetStretchToFit(false)
				remove_button:SetWide(16)
				remove_button.DoClick = function()
					local _, data = remove_entry:GetSelected()
					if data then classes[data] = nil end
				end
				remove_button:Dock(RIGHT)
			end
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("CreateMove", "Aimbot")
	end
}
