--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return {
	name = "Bunnyhop",
	description = "Automatically bunnyhops for you.",
	license = "CC0 1.0 Universal",
	icon = "icon16/sport_shuttlecock.png",
	start = function(filename, panel)
		local enabled = false
		hook_add("Think", "bhop", function()
			if enabled then
				if
					    (not gui.IsGameUIVisible())
					and (not gui.IsConsoleVisible())
					and (not LocalPlayer():IsTyping())
				then
					if
						    (LocalPlayer():OnGround())
						and (input.IsKeyDown(KEY_SPACE))
					then
						RunConsoleCommand("+jump")
					else
						RunConsoleCommand("-jump")
					end
				end
			end
		end)
		if panel and panel:IsValid() then
			local enabled_checkbox = vgui.Create("DCheckBoxLabel", panel)
			enabled_checkbox:SetValue(enabled)
			enabled_checkbox:SetText("Enabled")
			enabled_checkbox:SetDark(1)
			enabled_checkbox.OnChange = function(_, state)
				enabled = state
			end
			enabled_checkbox:DockMargin(0, 3, 0, 0)
			enabled_checkbox:Dock(TOP)
			
			panel:InvalidateChildren()
			panel:SizeToChildren(false, true)
		end
	end,
	stop = function()
		hook_remove("Think", "bhop")
	end
}
