--[[
To the extent possible under law, the author has dedicated all copyright
and related and neighboring rights to this software to the public domain
worldwide. This software is distributed without any warranty. You should
have received a copy of the CC0 Public Domain Dedication along with this
software. If not, see http://creativecommons.org/publicdomain/zero/1.0/.
]]
return function()
	brand = brand or "Clustercheat 2"
	version = version or "0.0.0"
	mod_path = mod_path or path.."modules/"
	commands_enabled = commands_enabled or true
	command_prefix = command_prefix or "cc_"
	commands = commands or {}
	hook_obfuscation = hook_obfuscation or false
	hook_prefix = hook_prefix or "CC_"..math.random(100, 10000).."_"
	hook_list = hook_list or {}
	stop_funcs = stop_funcs or {}
	-- These colors might change. Don't rely on them being constant.
	colors = colors or {
		white =     {255, 255, 255, 255},
		black =     {  0,   0,   0, 255},
		primary =   {  0, 123, 255, 255}, -- blue
		secondary = {108, 117, 125, 255}, -- blue grey
		success =   { 40, 167,  69, 255}, -- green
		danger =    {220,  53,  69, 255}, -- red
		warning =   {255, 193,   7, 255}, -- yellow
		info =      { 23, 162, 184, 255}  -- cyan
	}
	hook_add = hook_add or function(event, identifier, func)
		if hook_obfuscation then
			local hooks = hook.GetTable()
			if hooks[event] then
				-- This is inefficient but I don't think there's a better way.
				local options = {}
				local i = 1
				for identifier, func in pairs(hooks[event]) do
					options[i] = identifier
					i=i+1
				end
				local real_identifier = options[math.random(i)]
				hook_list[event] = hook_list[event] or {}
				hook_list[event][real_identifier] = {
					obfuscated = true,
					module_identifier = identifier,
					real_identifier = real_identifier,
					module_func = func,
					original_func = unpack(table.Copy({hooks[event][real_identifier]}))
				}
				hook.Add(event, real_identifier, function(...)
					hook_list[event][real_identifier].module_func(...)
					hook_list[event][real_identifier].original_func(...)
				end)
			else
				-- TODO: handle this properly
				return false
			end
		else
			hook_list[event] = hook_list[event] or {}
			hook_list[event][identifier] = {
				obfuscated = false,
				module_identifier = hook_prefix..identifier
			}
			hook.Add(event, hook_prefix..identifier, func)
			return true
		end
	end
	hook_remove = hook_remove or function(event, identifier)
		local target = hook_list[event]
		if target and target[identifier] then
			target = target[identifier]
			if target.obfuscated then
				hook.Add(event, target.real_identifier, target.original_func) 
			else
				hook.Remove(event, target.module_identifier)
			end
		end
	end
	command_add = command_add or function(name, callback, autocomplete, helptext, flags)
		commands[name] = true
		local final_name
		local first = name:sub(1, 1)
		if first == "+" then
			final_name = "+"..command_prefix..name:sub(2)
		elseif first == "-" then
			final_name = "-"..command_prefix..name:sub(2)
		else
			final_name = command_prefix..name
		end
		if not flags then flags = FCVAR_UNREGISTERED end
		concommand.Add(final_name, callback, autocomplete, helptext, flags)
	end
	command_remove = command_remove or function(name)
		commands[name] = nil
		local final_name
		local first = name:sub(1, 1)
		if first == "+" then
			final_name = "+"..command_prefix..name:sub(2)
		elseif first == "-" then
			final_name = "-"..command_prefix..name:sub(2)
		else
			final_name = command_prefix..name
		end
		concommand.Remove(final_name, callback, autocomplete, helptext, flags)
	end
	credits = credits or {
		brand,
		version,
		"Semi-discrete script loader for Garry's Mod.",
		"© 2019",
		"This software is distributed without any warranty."
	}
	
	mod_load = mod_load or function(filename, path)
		filename = tostring(filename)
		if stop_funcs[filename] then
			mod_list_remove(filename)
		end
		--local succ, data = pcall(include, filename)
		local code = file.Read(filename, path)
		local data
		local err
		if code then
			local func, lerr = CompileString(code, path..": "..filename, false)
			if func and type(func) == "function" then
				data = func()
			else
				err = lerr or func or "Unknown error"
			end
		end
		if code and data then
			local panel
			if data.start or data.stop then
				entry = mod_list_add(filename, data.stop, data, false)
			end
			if data.start then
				setfenv(data.start, env)
				pcall(data.start, filename, entry.panel)
			end
			if data.stop then
				stop_funcs[filename] = {
					func = data.stop,
					name = data.name,
					filename = tostring(filename)
				}
			end
			if data.name then
				print('Loaded module "'..tostring(data.name)..'" ('..filename..')')
			else
				print("Module probably loaded")
			end
		else
			if err then
				print("Failed to load module ("..filename..")")
				print("Error: "..tostring(err))
			else
				print("Failed to load module ("..filename..")")
			end
			return false
		end
	end
	
	mod_stop = mod_stop or function(filename)
		local stops = stop_funcs
		if not filename then
			print("Stop functions:")
			for k in pairs(stops) do
				print(k)
			end
		else
			if stops[filename] then
				setfenv(stops[filename].func, env)
				pcall(stops[filename].func, filename)
				if stops[filename].name then
					print('Stopped module "'..tostring(stops[filename].name)..'" ('..stops[filename].filename..')')
				else
					print("Stopped module ("..stops[filename].filename..")")
				end
				stops[filename] = nil
			else
				print("Stop function does not exist")
			end
		end
	end
	
	quit = quit or function()
		print("Quitting "..brand)
		if mod_list then
			for k, v in pairs(mod_list) do
				mod_list_remove(k)
			end
		end
		if commands then
			for command_name, _ in pairs(commands) do
				command_remove(command_name)
			end
		end
		if hooks then
			for event_name, event in pairs(hooks) do
				for identifier_name, _ in pairs(event) do
					hook_remove(event_name, identifier_name)
				end
			end
		end
	end
	
	local succ, func = pcall(include, path.."interface.lua")
	if succ and func then
		setfenv(func, env)
		func()
	else
		print("Failed to load interface")
	end
	
	print("Initialized "..brand)
end
