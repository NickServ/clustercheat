-- To the extent possible under law, the author has dedicated all
-- copyright and related and neighboring rights to this software to the
-- public domain worldwide. This software is distributed without any
-- warranty. You should have received a copy of the CC0 Public Domain
-- Dedication along with this software. If not, see
-- <http://creativecommons.org/publicdomain/zero/1.0/>.

return {
	name = "ClusterCheat frontend",
	start = function()
		local window = vgui.Create( "DFrame" )
		window:SetSize( 640, 480 )
		window:Center()
		window:SetScreenLock( true )
		window:SetMinWidth( 320 )
		window:SetMinHeight( 240 )
		window:SetSizable( true )
		window:SetTitle( "ClusterCheat menu" )
		window:MakePopup()
		
		local leftpane = vgui.Create( "Panel", window )
		leftpane:Dock( FILL )

		local total = vgui.Create( "DListView", leftpane )
		total:Dock( FILL )
		total:AddColumn( "Modules" )

		local rightpane = vgui.Create( "Panel", window )
		rightpane:Dock( RIGHT ) -- TODO: Make panes half and half
		rightpane:SetWidth( 160 ) -- instead of this ugly fixed width

		local loaded = vgui.Create( "DListView", rightpane )
		loaded:Dock( FILL )
		loaded:AddColumn( "Modules" )

		local refresh = function()
			total:Clear()
			-- "LUA" as second arg doesn't work for some reason??
			local ft = file.Find( prefixf.."*", "lsv" )
			if ft then
				for i=1, #ft do
					total:AddLine( ft[i] ).file = ft[i] -- lazy hack
				end
			end
			
			loaded:Clear()
			for k, v in pairs(stops) do
		--		PrintTable(v)
				loaded:AddLine( v.name ).file = v.file
			end
		end

		local loadbutton = vgui.Create( "DButton", leftpane )
		loadbutton:Dock( BOTTOM )
		loadbutton:SetHeight( 32 )
		loadbutton:SetText( "Start" )
		loadbutton.DoClick = function()
			local sel = total:GetSelected()
			if sel and #sel > 0 then
				for i=1, #sel do
					loadfunc( nil, nil, nil, sel[i].file )
				end
				refresh()
			end
		end

		local unloadbutton = vgui.Create( "DButton", rightpane )
		unloadbutton:Dock( BOTTOM )
		unloadbutton:SetHeight( 32 )
		unloadbutton:SetText( "Stop" )
		unloadbutton.DoClick = function()
			local sel = loaded:GetSelected()
			if sel and #sel > 0 then
				for i=1, #sel do
		--			print(sel[i].file)
					stopfunc( nil, nil, nil, sel[i].file )
				end
				refresh()
			end
		end

		window.OnClose = function()
			refresh = nil
		end

		refresh()
	end
}
