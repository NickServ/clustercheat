-- Speedometer
-- Created by me
--
-- To the extent possible under law, the author has dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.

return {
	name = "Speedometer",
	start = function()
		hook.Add("HUDPaint", prefixm.."ShowPos", function()
			-- TODO: Make this cooler and more useful
			draw.SimpleTextOutlined(
				math.floor( LocalPlayer():GetVelocity():Length() ),
				"DermaLarge", ScrW()/2, ScrH(), Color(unpack(color.white)), TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, 1, Color(unpack(color.black))
			)
		end)
	end,
	stop = function()
		hook.Remove("HUDPaint", prefixm.."ShowPos")
	end
}
