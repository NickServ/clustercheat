-- Flashlight Spam
-- Created by me
--
-- To the extent possible under law, the author has dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
return {
	name = "Flashlight Spam",
	start = function()
		concommand.Add("+"..prefixc.."lightspam", function()
			hook.Add("Think", prefixm.."flashlightspam", function()
				RunConsoleCommand("impulse", "100")
			end)
		end)
		concommand.Add("-"..prefixc.."lightspam", function()
			hook.Remove("Think", prefixm.."flashlightspam")
		end)
	end,
	stop = function()
		concommand.Remove("+"..prefixc.."lightspam")
		concommand.Remove("-"..prefixc.."lightspam")
	end
}
