-- CalcView FOV
-- Created by me
-- 
-- To the extent possible under law, the author has dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
-- You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
return {
	name = "CalcView FOV",
	start = function()
		fovcv = CreateClientConVar( prefixc.."fov", 110, true, false, "Changes your FOV" )
		hook.Add( "CalcView", prefixm.."calcview", function(ply, origin, angles, fov, znear, zfar)
			return {
				origin,
				angles,
				fovcv:GetInt(),
				znear,
				zfar
				false,
				nil
			}
		end )
	end, 
	stop = function()
		fovcv = nil -- TODO: Figure out how to destroy a ConVar
		hook.Remove( "CalcView", prefixm.."calcview" )
	end
}
