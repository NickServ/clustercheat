-- To the extent possible under law, the author has dedicated all
-- copyright and related and neighboring rights to this software to the
-- public domain worldwide. This software is distributed without any
-- warranty. You should have received a copy of the CC0 Public Domain
-- Dedication along with this software. If not, see
-- <http://creativecommons.org/publicdomain/zero/1.0/>.

return function()
	name = "ClusterCheat"
	prefixc = "cc_"
	prefixf = path.."modules/"
	prefixm = "CLUSTERCHEAT_" -- to prevent collisions
	stops = {}
	
	color = {
		white =     {255, 255, 255},
		black =     {  0,   0,   0},
		primary =   {  0, 123, 255}, -- blue
		secondary = {108, 117, 125},
		success =   { 40, 167,  69}, -- green
		danger =    {220,  53,  69}, -- red
		warning =   {255, 193,   7}, -- yellow
		info =      { 23, 162, 184} -- blue
	}
	conf = {
		printer = color.warning,
		shipment = color.primary,
		money = color.success
	}
	
	loadfunc = function(ply, cmd, args, argStr, external)
		local prefixf = prefixf
		if external then prefixf = "" end
		
		local succ, data = pcall(include, prefixf..tostring(argStr))
		if data then
			if data.start then
				setfenv(data.start, env)
				ProtectedCall(data.start)
			end
			if data.stop then
				stops[argStr] = {
					func = data.stop,
					name = data.name,
					file = argStr
				}
			end
			if data.name then
				print('"'..tostring(data.name)..'" probably loaded')
			else
				print("Module probably loaded")
			end
		else
			print("Failed to load module")
		end
	end
	loadefunc = function(ply, cmd, args, argStr)
		loadfunc(ply, cmd, args, argStr, true)
	end
	
	concommand.Add(prefixc.."load", loadfunc, nil, "Load a module", FCVAR_UNREGISTERED)
	concommand.Add(prefixc.."loade", loadefunc, nil, "Load an external module", FCVAR_UNREGISTERED)
	
	stopfunc = function(ply, cmd, args, argStr)
		if argStr and argStr ~= "" then
			if stops[argStr] then
				setfenv(stops[argStr].func, env)
				ProtectedCall(stops[argStr].func)
				if stops[argStr].name then
					print('"'..tostring(stops[argStr].name)..'" probably stopped')
				else
					print("Module probably stopped")
				end
				stops[argStr] = nil
			else
				print("There is no stop function for that module")
			end
		else
			for k in pairs(stops) do
				print(k)
			end
		end
	end
	concommand.Add(prefixc.."stop", stopfunc, nil, "If possible, stop a module", FCVAR_UNREGISTERED)
	
	concommand.Add(prefixc.."eval", function(ply, cmd, args, argStr)
		local succ, ret = pcall(RunString, argStr, name)
		print(tostring(succ), tostring(ret))
	end, nil, "Run code inside the cheat environment", FCVAR_UNREGISTERED)
	
	print(name.." initialized")
end
