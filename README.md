# Clustercheat
![Screenshot of menu](screenshots/cc2_menu.png)
![Screenshot of ESP](screenshots/cc2_esp.png)

I know the above shots look glamorous but this cheat sucks.
If you think I'm a skid now, you should see how much of a skid I was when I made this.
## Clustercheat 1
Uses console commands for the interface.
Apparently there was an interface too that I forgot I made.
Most of the modules were pasted and I only posted what *I* made for legal reasons, so it's pretty lacking in features.
**Not tested in many months!**
## Clustercheat 2
A slightly better but still shitty version.
This one has a working Derma interface, and it's the one being shown in the screenshots.
Load modules with File->Open, unload by clicking "Stop," and quit with File->Quit.
This one isn't pasted so I can legally post all the modules here because *I* made them.
Press backslash to open menu.
Load `init_insecure.lua` instead of `init.lua` if you want to avoid script errors and crashes and shit.
# Included modules
## Clustercheat 1

- `esp_fov.lua` - FOV hack; set `cc_fov` to desired FOV.
- `esp_speedometer.lua` - Displays speedometer at bottom of screen.
- `misc_gui.lua` - Probably broken GUI? Not tested.
- `misc_lightspam.lua` - Flashlight spammer; bind a key to `+cc_lightspam`.
## Clustercheat 2
Dropdowns with entities or players in them have to be right-clicked to updated because I couldn't be bothered to make it auto-update.

- `aimbot.lua` - Targets everyone by default, if you add people to target list it will only target people on list. Set to *Spine 3* for best results.
- `antiaim.lua` - Broken anti-aim. Don't even bother.
- `autocollect.lua` - Picks up dropped items on Meta Construct. Can't be bothered to make it work better.
- `bhop.lua` - Hold space to do it sideways.
- `chams.lua` - Doesn't change colors. If you enable *Only show through walls* it bugs out, so consider also checking *Suppress lighting* if you do that.
- `esp.lua` - IMO kinda nice looking ESP. Can display custom text for specific SteamIDs as defined in `esp_text.json`.
- `freecam.lua` - Use checkbox to turn on/off. Stuff too far away isn't sent to your client and I can't do anything about that.
- `lightspam.lua` - Use checkbox to turn on/off. Ear rape.
- `waila.lua` - Gives tons of info about what you're looking at.
# Make your own module
## Clustercheat 1
Return a table containing the keys `name`, `start`, and `stop`.
Name should be a string, and start and stop will be functions.
Start gets called when the module is first loaded, stop gets called when the user tries to unload it.
Prefix commands or convars with `prefixc`. There are some colors defined in `color` in the form of arrays with 3 values in RGB.
For an example, look at `misc_lightspam.lua`.
## Clustercheat 2
Same as CC1, but the start function receives its own filename and a panel, and also the following:
When adding and removing hooks, use `hook_add` and `hook_remove` instead of `hook.Add` and `hook.Remove` so that the cheat can use obfuscated hook names.
When adding stuff to the panel, do `panel:InvalidateChildren()` and `panel:SizeToChildren(false, true)` afterwards to make it properly sized.
# License
To the extent possible under law, the author has dedicated all copyright and related and neighboring rights to this software to the public domain worldwide.
This software is distributed without any warranty.
You should have received a copy of the CC0 Public Domain Dedication along with this software.
If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
